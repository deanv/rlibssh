Welcome! This is my personal pet project to create a functional ssh library written in rust.
I understand that there is another rust library called "thrussh" floating around in the ether
but I have chosen not to use this project because it is unmaintained and has not been committed
to in over a year now.

This library will take inspiration in API calls from the libssh C library, and when in doubt, will
have a similar architecture. Though it will make no claims to be exactly the same, rlibssh is made in
the hopes that if you have used libssh before you will learn the api of rlibssh in no time.

Rlibssh is obviously in the very early development stages and should not be used in security critical
applications quite yet. Hopefully, however, there will come a time when, by the nature of the language,
it is more thoroughly vetted than any other library or implemmentation out there, including openssh 
and libssh.
